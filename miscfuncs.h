#include <stdio.h>

// How many characters the buffer lines might length.
#define PINGLINEBSIZE     100

#define GLOBALS_INITIALIZER     (struct _globals) {NULL, NULL, NULL, 0, NULL, {0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}, {0, 0, 0, 0}, 0}
#define PRINT_OCTAVE(x)         fputs (x"\n", globals->pipe_octave);

/**
 * Global variables encapsulated in a struct. The main .c file should have a static instance of this.
 */
struct _globals
{
    // Pipes
    FILE*   pipe_ping;
    FILE*   pipe_octave;

    // Plot title
    char*   plot_title;
    int     argc_copy;
    char**  argv_copy;

    // Math variables
    double  last_samples [2];   //last_samples[0] is the last sample.
    double  estimator_avg [2];  //estimator_avg[0] is the last sample.
    double  estimator_sd [2];   //estimator_sd[0] is the last estimator.

    int     delayed_samples [4];    // Counter for delayed packets with variable N's.
    int     samples_retrieved;
};

/**
 * Prints usage tips.
 * @param	programname		Used to print this program's name (deploy argv[0] to this function)
 */

void printhelp (char* programname);

/**
 * Parses a text line returned by PING. Not too robust. Try not to put random text here.
 * @param   pingline        A line read by fgets from a "ping" pipe.
 *
 * @return  The amount of milliseconds a package took to return. A negative number if it was not a valid line.
 */

double parsepingline (char* pingline);

/**
 * Opens a pipe to PING command.
 *
 * @param   host            Argument supplied to PING command.
 *
 * @return  A FILE* stream representing an open pipe for this program to read to.
 */

FILE* openpingpipe (char* host);

/**
* Waits for a ping line, then processes it and changes all corresponding variables.
*
* @param    globals         Pointer to the global variables.
*/

void gettwopinglines (struct _globals* globals);

/**
* Updates the plots made in Octave.
*
* @param    globals         Pointer to the global variables.
*/
void updateplots (struct _globals* globals);

/**
* Tells if the last package was lost.
*
* @param    globals         Pointer to the global variables.
* @param    N               Amount of standard deviations used.
*
* @return   0 if not lost, 1 if lost.
*/
int lastpackagelost (struct _globals* globals, int N);
