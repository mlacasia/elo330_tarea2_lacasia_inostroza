#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "miscfuncs.h"

#define     ABSV(x)     (x < 0) ? (-x) : (x)

// Octave commands.
#define     RTTPATTERN      "x = [x, %.4lf, %.4lf];\n"
#define     SDPATTERN       "sd = [sd, %.4lf, %.4lf];\n"
#define     AVGPATTERN      "avg = [avg, %.4lf, %.4lf];\n"

void printhelp (char* programname)
{
	printf (
"\n\
*************************************************************************************\n\
* par: Audio packet loss due delays.                                                *\n\
* Uso: %s <ping server> <smtp server> <email> <codified authentication>            \n\
*************************************************************************************\n\n\
", programname);
}

double parsepingline (char* pingline)
{
    char*   modifiedline;
    char*   token;
    int     linecomparing;

    // Copies the line to use in a new string called modifiedline, to keep the argument "pingline" constant.
    modifiedline =  strdup (pingline);

    // Tokenizes the line separating in spaces. If a token "time=XXXX" is found, this function returns XXXX as a double value,
    // where XXXX is the packet delay in milliseconds.
    token = strtok (modifiedline, " ");

    do
    {
        // A token called "time" was found.
        if (memcmp (token, "time", 4) == 0)
        {
            free (modifiedline);
            // It may seem to be a weird way to do this. It simply tells that should begin to convert from the 6th letter (omitting the "time=" part)
            // of the string instead of the first.
            return (strtod (&(token[5]), NULL));
        }
    } while((token = strtok (NULL, " ")) != NULL);

    // Returns -1 if nothing was found.
    free (modifiedline);

    return -1.0;
}

FILE* openpingpipe (char* host)
{
    char* pipecmd;
    FILE* pipestream;
    const char pingpattern[] = "ping -n \"%s\"";

    asprintf (&pipecmd, pingpattern, host);
    pipestream = popen (pipecmd, "r");
    free (pipecmd);

    return pipestream;
}

void gettwopinglines (struct _globals* globals)
{
    char    pingline [PINGLINEBSIZE + 1];
    double  lastpingvalue;

    int     c;
    int     nrange;

    // Does this twice.
    for (c = 0; c < 2; c++)
    {
        // Retrieves a ping line. This should take 1 second.
        fgets (pingline, PINGLINEBSIZE, globals->pipe_ping);
        lastpingvalue = parsepingline (pingline);

        // Conveyor belt for these values.
        globals->estimator_avg[1] = globals->estimator_avg[0];
        globals->estimator_sd[1] =  globals->estimator_sd[0];
        globals->last_samples[1] =  globals->last_samples[0];

        // Update the values.
        globals->estimator_avg[0] = 0.98 * globals->estimator_avg[0] + 0.02 * lastpingvalue;
        globals->estimator_sd[0] =  0.75 * globals->estimator_sd[0] + 0.25 * ABSV (lastpingvalue - globals->estimator_avg[0]);
        globals->last_samples[0] = lastpingvalue;

        globals->samples_retrieved ++;

        for (nrange = 1; nrange <= 4; nrange++)
            globals->delayed_samples[nrange - 1] += lastpackagelost(globals, nrange);
    }
}

void updateplots (struct _globals* globals)
{
    // Throw commands to Octave.
    fprintf (globals->pipe_octave, RTTPATTERN, globals->last_samples[1], globals->last_samples[0]);
    fprintf (globals->pipe_octave, AVGPATTERN, globals->estimator_avg[1], globals->estimator_avg[0]);
    fprintf (globals->pipe_octave, SDPATTERN, globals->estimator_sd[1], globals->estimator_sd[0]);

    PRINT_OCTAVE ("clf")

    PRINT_OCTAVE ("hold on")
    PRINT_OCTAVE ("k = plot (1:size(x, 2), x)")
    PRINT_OCTAVE ("legend (k, 'Packet RTT')")

    PRINT_OCTAVE ("l = plot (1:size(x, 2), avg, 'r')")
    PRINT_OCTAVE ("legend (l, 'Average estimator')")

    PRINT_OCTAVE ("ylabel ('Delay [ms]')")
    PRINT_OCTAVE ("xlabel ('Packet number [n]')")

    PRINT_OCTAVE ("grid on")

    PRINT_OCTAVE ("hold off")

    fprintf (globals->pipe_octave, "title ('RTT time for packets sent to %s')\n", globals->plot_title);

    fflush (globals->pipe_octave);
}

int lastpackagelost (struct _globals* globals, int N)
{
    double threshold;

    threshold = globals->estimator_avg[0] + N * globals->estimator_sd[0];

    if (globals->last_samples[0] > threshold)
        return 1;
    else
        return 0;
}
