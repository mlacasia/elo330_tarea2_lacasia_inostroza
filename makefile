CC=gcc

SOURCES=par.c miscfuncs.o
OBJECTS=par.o miscfuncs.o
HEADERS=miscfuncs.h
CFLAGS= -g
LFLAGS=

all:		par

par:		$(OBJECTS)
			$(CC) $^ -o $@ $(LFLAGS)

par.o:		par.c $(HEADERS)
			$(CC) $(CFLAGS) -c $< -o $@

miscfuncs.o:	miscfuncs.c $(HEADERS)
			$(CC) $(CFLAGS) -c $< -o $@

clean:
			rm -rf *.o par octave-core
