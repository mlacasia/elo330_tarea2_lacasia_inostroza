#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "miscfuncs.h"

#define LINE_SIZE   200
#define OCTAVECMD   "octave -i"
#define OCTAVEINIT  "\nx = []\n sd = []\n avg = []\n"

static void     atquit (int signo);
void            clean_globals ();

// This variable holds them all.
static struct _globals globals;

int main (int argc, char** argv)
{
    int argvcnt;
    int debugcnt = 0;

    // Initialize global variables.
    globals = GLOBALS_INITIALIZER;

    //if (argc != 5)
    if (argc != 5)
    {
        printhelp (argv[0]);
        exit (1);
    }

    // Opens both pipes.
    globals.pipe_ping = openpingpipe (argv[1]);
    if (!(globals.pipe_ping))
    {
        fprintf (stderr, "Could not open a pipe to the \"ping\" command.\n");
        exit (-1);
    }

    globals.pipe_octave = popen (OCTAVECMD, "w");
    if (!(globals.pipe_octave))
    {
        fprintf (stderr, "Could not open a pipe to the \"octave\" command.\n");
        exit (-1);
    }

    // Initializes the plot title.
    globals.plot_title = strdup (argv[1]);

    // Copies argv into a global variable.
    globals.argc_copy = argc;
    globals.argv_copy = malloc (argc * sizeof (char*));
    for (argvcnt = 0; argvcnt < argc; argvcnt++)
        globals.argv_copy[argvcnt] = strdup (argv[argvcnt]);

    // Initializes Octave variables as empty vectors
    fputs (OCTAVEINIT, globals.pipe_octave);

    if (signal(SIGINT, atquit) == SIG_ERR)
    {
        fprintf (stderr, "Cannot catch SIGINT signal.\n");
        exit (-1);
    }

    // Infinite loop processing all the "ping" inputs.
    while (1){
	gettwopinglines (&globals);
        updateplots (&globals);
    }

    atquit (SIGINT);
    exit (2);
}

/*
 * Function called when pressing CTRL+C.
 */
static void atquit (int signo)
{

    printf ("\n\n********************************************************************\n");
    printf ("Total sent packages: %d\n", globals.samples_retrieved);
    printf ("--------------------------------------------------------------------\n");
    printf ("Amount of lost packages with N = 1: %d\n", globals.delayed_samples[0]);
    printf ("Amount of lost packages with N = 2: %d\n", globals.delayed_samples[1]);
    printf ("Amount of lost packages with N = 3: %d\n", globals.delayed_samples[2]);
    printf ("Amount of lost packages with N = 4: %d\n", globals.delayed_samples[3]);
    printf ("********************************************************************\n");

  pid_t pid;
  int pfdToChild[2];
  int pfdFromChild[2];
  int i, status;
  char* smtp;
  int port;
  char* auth;
  char* rcpt_to;
  FILE * toChild, *fromChild;
  char line[LINE_SIZE];

  float N1delayed =(((float)globals.delayed_samples[0])/((float)globals.samples_retrieved))*100;
  float N2delayed =(((float)globals.delayed_samples[1])/((float)globals.samples_retrieved))*100;
  float N3delayed =(((float)globals.delayed_samples[2])/((float)globals.samples_retrieved))*100;
  float N4delayed =(((float)globals.delayed_samples[3])/((float)globals.samples_retrieved))*100;

  port = 587;
  smtp = globals.argv_copy[2];
  rcpt_to = globals.argv_copy[3];
  auth = globals.argv_copy[4];

    /* Create a pipe. */
  if (pipe(pfdToChild) < 0) {
    perror("pipe");
    exit(1);
  }
  if (pipe(pfdFromChild) < 0) {
    perror("pipe");
    exit(1);
  }

    /* Create a child process. */
  if ((pid = fork()) < 0) {
    perror("fork");
    exit(1);
  }

    /* The child process executes "openssl" to create a secure conection. */
  if (pid == 0) {
      /* Attach standard input to the end of the pipe where parent writes. */
    dup2(pfdToChild[0], 0);  //parent process -> openssl
    close(pfdToChild[1]);
      /* Attach standard output to the end of the pipe where parent reads. */
    dup2(pfdFromChild[1],1);//openssl -> parent
    dup2(pfdFromChild[1],2);//The error/success codes of each smtp command go to standard error
    close(pfdFromChild[0]);
    sprintf(line,"%s:%i",smtp,port);//variable arguments of openssl
    execl("/usr/bin/openssl", "openssl", "s_client",
          "-starttls","smtp", "-crlf", "-connect", line, NULL);
    perror("exec");
    _exit(127);
  }
   /* This is the parent */
  close(pfdToChild[0]);
  close(pfdFromChild[1]);
  toChild = fdopen(pfdToChild[1], "w");
  fromChild = fdopen(pfdFromChild[0], "r");
    /* Write our mail message to the pipe.*/  
    /* wait for the end of the key exchange mechanism */
  while (strncmp(fgets(line, sizeof(line), fromChild),"250 ",4)!=0);
  fprintf(toChild,"AUTH PLAIN %s\n", auth);
  fflush(toChild);
  fgets(line, sizeof(line), fromChild);
  printf("\tUser AUTH\t%s\n", line);
  fprintf(toChild,"mail from: <%s>\n",rcpt_to);
  fflush(toChild);
  fgets(line, sizeof(line), fromChild);
  printf("\tUser mail from\t%s\n", line);
  fprintf(toChild,"rcpt to: <%s>\n",rcpt_to);
  fflush(toChild);
  fgets(line, sizeof(line), fromChild);
  printf("\tUser rcpt to\t%s\n", line);
  fprintf(toChild,"data\n");
  fflush(toChild);
  fgets(line, sizeof(line), fromChild);
  printf("\tUser data\t%s\n", line);
  fprintf(toChild, "From:\"Me\" <%s>\n\
To: \"Me\" <%s>\nSubject: paquetes atrasados\n\n\
Servidor: %s\nN		%%Paquetes Atrasados\n1		%f%%\n2		%f%%\n3		%f%%\n4		%f%%\n.\n",rcpt_to,rcpt_to,globals.argv_copy[1],N1delayed,N2delayed,N3delayed,N4delayed);
  fflush(toChild);
  fflush(fromChild);
  fgets(line, sizeof(line), fromChild);
  printf("\tUser\t%s\n", line);
  fprintf(toChild,"quit\n");
  fflush(toChild);
  fgets(line, sizeof(line), fromChild);
  printf("\tUser quit\t%s\n", line);
    /* Close the pipe and wait for the child to exit. */
  fclose(toChild);
  fclose(fromChild);
  waitpid(pid, &status, 0);
    /* Exit with a status of 0, indicating that everything went fine. */
    clean_globals ();
    exit (0);
}

void clean_globals ()
{
    int argvcnt;

    free (globals.plot_title);
    for (argvcnt = 0; argvcnt < globals.argc_copy; argvcnt++)
        free (globals.argv_copy[argvcnt]);

    free (globals.argv_copy);

    pclose (globals.pipe_octave);
    pclose (globals.pipe_ping);
}
